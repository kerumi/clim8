import { Request, Response } from "express";
import { weeronline } from "../resources/response.json";
import { Weather } from "../types/weather";
import { getWeatherDescription, getWeatherIcon } from "../utils";

/** Parses the weather info from weeronline into custom forecast app format */
export const parseWeather = ({ location, weather }: any): Weather => ({
  location: {
    city: location["city_name"],
    country: location["country_name"]
  },
  weather: {
    id: weather["_id"],
    date: new Date(weather["forecast_date"]),
    minTemperature: weather["temperature_min"],
    maxTemperature: weather["temperature_max"],
    precipitationAmount: weather["precipitation_amount"],
    precipitationPercentage: weather["precipitation_percentage"],
    temperatureMorning: weather["temperature_morning"],
    temperatureAfternoon: weather["temperature_afternoon"],
    temperatureNight: weather["temperature_night"],
    temperatureFeelsLike: weather["feels_like_temperature_max"],
    weatherIcon: getWeatherIcon(weather["weather_symbol"]),
    weatherIconAfternoon: getWeatherIcon(weather["weather_symbol_afternoon"]),
    weatherIconMorning: getWeatherIcon(weather["weather_symbol_morning"]),
    weatherIconNight: getWeatherIcon(weather["weather_symbol_night"]),
    weatherDescription: getWeatherDescription(weather["weather_symbol"]),
    weatherDescriptionAfternoon: getWeatherDescription(
      weather["weather_symbol_afternoon"]
    ),
    weatherDescriptionMorning: getWeatherDescription(
      weather["weather_symbol_morning"]
    ),
    weatherDescriptionNight: getWeatherDescription(
      weather["weather_symbol_night"]
    ),
    windSpeed: weather["wind_speed_bft"]
  }
});

/** Sends a current weather from fake weeronline response */
export function getWeather(req: Request, res: Response) {
  res.send(
    parseWeather({ location: weeronline.geoinfo, weather: weeronline.data[0] })
  );
}
