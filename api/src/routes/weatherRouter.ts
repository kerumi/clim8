import { Router } from "express";
import { getWeather } from "../controllers/getWeather";

const weatherRouter = Router();

weatherRouter.get("/", getWeather);

export default weatherRouter;
