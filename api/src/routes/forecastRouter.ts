import { Router } from "express";
import { getForecast } from "../controllers/getForecast";

const forecastRouter = Router();

forecastRouter.get("/", getForecast);

export default forecastRouter;
