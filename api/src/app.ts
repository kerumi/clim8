import express from "express";
import api from "./api";
import cors from "cors";

const app = express();

app.use(cors());
app.use("/api", api);

export default app;
