## Clim8 Weather/Forecast Fake API

This is a simple nodejs app that provides a custom format fake weather response from weeronline.

## Dependencies

- ExpressJS
- ExpressJS Middlewares
  - Compression - https://github.com/expressjs/compression
  - Helmet - https://helmetjs.github.io/
- TypeScript
- NPM or Yarn

## Install, Build, Run

Install node package dependencies:

`$ npm install`

Build & Run ExpressJS Server:

`$ npm start`

Run Unit Tests:

`$ npm test`
