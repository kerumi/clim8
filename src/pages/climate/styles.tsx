/** @jsx jsx */
import { css, jsx } from "@emotion/core";

export const spinnerItem = css`
  -webkit-box-flex: 0;
  -ms-flex: 0 0 auto;
  flex: 0 0 auto;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -ms-flex-direction: column;
  flex-direction: column;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  padding: 20px;
  margin-top: 175px;
  font-size: 18px;
  letter-spacing: 1px;
  position: relative;
`;

export const SpinnerItem = (props: any) => <div css={spinnerItem} {...props} />;
