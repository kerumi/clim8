import { createRequest } from "./config";

const request = createRequest();

export const getWeather = () => {
  return request.get("/weather");
};
