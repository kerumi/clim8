import axios from "axios";

export const createRequest = () => {
  return axios.create({
    timeout: 10000,
    baseURL: `${process.env.REACT_APP_API_ENDPOINT}/api`
  });
};
