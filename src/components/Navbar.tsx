import React from "react";

/** Renders the navbar with a brand logo on it */
const Navbar: React.FC = () => {
  return (
    <nav className="navbar navbar-expand navbar-dark mb-3">
      <a href="/" className="navbar-brand">
        <img alt="brand-logo" src="assets/logo.svg" />
      </a>
    </nav>
  );
};

export default Navbar;
