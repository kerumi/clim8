import React, { PureComponent } from "react";
import { Weather as WeatherType } from "../types/weather";

type Props = {
  /** displays the current weather info */
  info: WeatherType;
};

/** renders the current weather description  */
const WeatherDescription: React.FC = ({ dayTime, icon, description }: any) => {
  return (
    <div className="col mb-2">
      <h2 className="text-white text-center"> {dayTime} </h2>
      <img
        src={`/assets/${icon}`}
        alt="Weather icon"
        className="m-auto"
        id="current-weather-icon"
      />

      <h4 className="text-white text-center text-capitalize mt-3">
        {description}
      </h4>
    </div>
  );
};

/** renders the specs for the current weather today */
class Weather extends PureComponent<Props> {
  render() {
    const { location, weather } = this.props.info;
    const forecast = [
      {
        dayTime: "Morning",
        icon: weather.weatherIconMorning,
        description: weather.weatherDescriptionMorning
      },
      {
        dayTime: "Afternoon",
        icon: weather.weatherIconAfternoon,
        description: weather.weatherDescriptionAfternoon
      },
      {
        dayTime: "Night",
        icon: weather.weatherIconNight,
        description: weather.weatherDescriptionNight
      }
    ];
    return (
      <div className="weather">
        <div className="card mb-4 opacity">
          <div className="card-body">
            <h1 className="text-center text-white text-capitalize">
              {location.city}, {location.country}
            </h1>

            <h2 className="text-white text-center" style={{ fontSize: "60px" }}>
              {weather.minTemperature}&deg; / {weather.maxTemperature}&deg;
            </h2>

            <h5 className="text-white text-center m-3">
              {new Date(weather.date).toLocaleDateString("en-US", {
                weekday: "long",
                month: "long",
                day: "numeric"
              })}
            </h5>
            <div className="row">
              {forecast.map((weather, i) => (
                <WeatherDescription key={i} {...weather} />
              ))}
            </div>

            <p className="text-white text-center">
              <span className="p-3">Wind {weather.windSpeed} m/s</span>
              <span className="p-3">
                Precipitation {weather.precipitationPercentage}%
              </span>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Weather;
