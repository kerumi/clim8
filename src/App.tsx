import React from "react";
import "./App.css";
import Climate from "./pages/climate";
import Navbar from "./components/Navbar";

/** Renders the Main Climate App */
const App: React.FC = () => (
  <div>
    <Navbar />
    <div className="container">
      <Climate />
    </div>
  </div>
);

export default App;
