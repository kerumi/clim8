import { combineReducers } from "redux";
import weatherReducer from "./weather";
import forecastReducer from "./forecast";

const rootReducer = combineReducers({ weatherReducer, forecastReducer });

export default rootReducer;
