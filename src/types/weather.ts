import { Location } from "./location";
import { Forecast } from "./forecast";

export interface Weather {
  location: Location;
  weather: Forecast;
}
