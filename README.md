## Clim8 Single Page Application

This is a simple web app that displays the current weather and forecast based on weeronline fake clim8 API.

## Dependencies

- React/Redux
- NodeJS v9+
- Emotion
- ExpressJS
- TypeScript
- NPM or Yarn

## Getting Started

### Running the Backend

`$ cd ./api/`

Install node package dependencies:

`$ npm install`

Build & Run Backend Server:

`$ npm start`

Run Unit Tests (optional):

`$ npm test`

### Running the Frontend

On root folder:

`$ npm install`

Run App:

`$ npm start`

Run Unit Tests (optional):

`$ npm test`

### Config

To change the fake weather information response, it can be changed based on the resources at
`./api/resources/response.json` & `./api/resources/weatherSymbols.json`

> Developed by KelviNosse in approx 4hours
